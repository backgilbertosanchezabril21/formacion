package net.techu;

import io.swagger.annotations.ApiOperation;
import net.techu.data.ProductoMongo;
import net.techu.data.ProductoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ProductosSwaggerController {
    @Autowired
    private ProductoRepository repository;

    @PostMapping(value = "/v4/productosCabecera")
    @ApiOperation(value="Crear producto", notes="Este método crea un producto")
    public ResponseEntity<String> addProductoCabecera(
            @RequestHeader(value = "usuario", required = false) String usuario,
            @RequestHeader(value = "password", required = false) String password,
            @RequestBody ProductoMongo productoMongo){

        ProductoMongo resultado = new ProductoMongo();
        if(usuario == null){
            System.out.println("Falta el usuario");
        }else if(password == null){
            System.out.println("Falta el pass");
        }else {
            resultado = repository.insert(productoMongo);
        }
        return new ResponseEntity<String>(resultado.toString(), HttpStatus.OK);
    }
}
